module TareaSpec (spec) where
import Test.Hspec
import Tarea

spec :: Spec
spec =
  describe "Pruebas para la tarea" $ do

    it "mapMaxDelPar" $
      mapMaxDelPar [(2,8),(-7,-3),(0,-1)] `shouldBe` [8,-3,0]

    it "mapEsPar" $
      mapEsPar [2,3,7,4,4] `shouldBe` [True,False,False,True,True]
  
    it "primeras" $
      primeras [(4,3),(2,9),(9,2)] `shouldBe` [4,2,9]

    it "boolsToBins" $
      boolsToBins [True,False,False,False] `shouldBe` [1,0,0,0]

    it "binsToBools" $
      binsToBools [0,1,1] `shouldBe` [False,True,True]

    it "numsToDiasDeLaSemana" $
      numsToDiasDeLaSemana [1,4] `shouldBe` ["Lunes","Jueves"]

