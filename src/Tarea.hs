module Tarea where

-- Dada una lista de pares de numeros, devuelve una lista con el mayor de cada par
mapMaxDelPar :: [(Int,Int)] -> [Int]
mapMaxDelPar = undefined


-- Dada una lista de numeros, devuelve una lista de True/False que indica si
-- los numeros son pares o no
mapEsPar :: [Int] -> [Bool]
mapEsPar = undefined


-- Dada una lista de pares, devuelve una lista con las primeras componentes de cada par
primeras :: [(a,b)] -> [a]
primeras = undefined


-- Dada una lista de Boolean, devuelve una lista de ceros y unos, cero si es False
-- uno si es True
boolsToBins :: [Bool] -> [Int]
boolsToBins = undefined


-- Dada una lista de ceros y unos, devuelve una lista de False/True respectivamente
-- Parcial si la lista contiene otros numeros
binsToBools :: [Int] -> [Bool]
binsToBools = undefined


-- Dada una lista de numeros del 0 al 6, devolver una lista de Strings
-- con los nombres de la semana a los que corresponde cada día
-- Los nombres son: Domingo, Lunes, Martes, Miércoles, Jueves, Viernes y Sábado
numsToDiasDeLaSemana :: [Int] -> [String]
numsToDiasDeLaSemana = undefined
