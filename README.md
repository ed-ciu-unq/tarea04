# FRANKENSTEIN: Estructuras + Haskell + git

El proyecto Frankenstein consiste en una cantidad de tareas, en orden creciente de dificultad, que cubren el contenido de la parte de Haskell de Estructuras de Datos.

El objetivo principal en Frankenstein consiste en “matar los _undefined_”. Un _undefined_ representa un espacio en blanco que se debe completar con código que cumpla la funcionalidad esperada.

El comando `stack test` permite correr los tests de la tarea. Al principio todos ellos deben estar en rojo. La meta es que todos pasen a estar en verde.

## Tarea 04: Maps sobre listas

En las funciones que usan el patrón MAP, la cantidad de los elementos de entrada es la misma que de salida, pero cada uno de los elementos ha sido transformado de alguna forma.

### Nota:

No duden de crear sus propias funciones auxiliares

---

Autor: Román García (nykros@gmail.com)
